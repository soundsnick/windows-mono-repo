export type Icon = {
  readonly width: number;
  readonly height: number;
  readonly path: string | ReadonlyArray<string>;
};
