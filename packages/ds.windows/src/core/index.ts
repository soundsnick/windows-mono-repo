import * as global from './global';
import * as iconsCore from './icons/core';
import * as icons from './icons/pack';

export { global, iconsCore, icons };
