import { css, SerializedStyles } from '@emotion/core';

export const styles = (): SerializedStyles => css`
  // google open sans font, weights: 400, 600, 700
  @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap');

  html,
  body {
    height: 100%;
    color: #282828;
    background-color: #fff;
    font-size: 15px;
  }

  // Common root element id for react applications:
  // cra, storybook.
  #root {
    height: 100%;
    display: flex;
    flex-direction: column;
  }

  p,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 0;
  }
`;
