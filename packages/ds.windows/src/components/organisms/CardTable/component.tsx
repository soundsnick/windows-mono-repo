import { Grid } from 'components';
import React, { FC } from 'react';

import { Props } from './props';

export const CardTable: FC<Props> = ({ children, columns, gap, title, ...rest }: Props) => (
  <div {...rest}>
    <h1>{title}</h1>
    <Grid columns={columns} gap={gap}>
      {children}
    </Grid>
  </div>
);
