import { CardProps, GridProps } from 'components';
import { HTMLAttributes, ReactElement } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'children' | 'title'> & {
  readonly children: ReadonlyArray<ReactElement<CardProps>>;
  readonly title: string;
} & GridProps;
