import { Card } from 'components';
import React, { FC } from 'react';

import { CardTable } from './component';

export default {
  title: 'CardTable',
};

export const CardTableBasic: FC = () => (
  <div>
    <CardTable title="Welcome" columns={2} gap={10}>
      <Card className="mb-3" link="https://vk.com" linkLabel="ВКонтакте">
        ВКонтакте
      </Card>
      <Card className="mb-3" link="https://twitter.com" linkLabel="Twitter">
        Twitter
      </Card>
    </CardTable>
  </div>
);
