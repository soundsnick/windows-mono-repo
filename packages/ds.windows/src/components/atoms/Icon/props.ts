import { iconsCore } from 'core';
import { SVGProps } from 'react';

import { Sizes } from './types/Sizes';

export type Props = SVGProps<SVGSVGElement> & {
  readonly color?: string;
  readonly icon: iconsCore.Icon;
  readonly size?: Sizes;
};
