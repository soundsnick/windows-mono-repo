import { iconsCore } from 'core';
import { createElement, ReactElement } from 'react';

export const createSvgPath = (
  path: iconsCore.Icon['path'],
): ReactElement | ReadonlyArray<ReactElement> =>
  Array.isArray(path)
    ? path.map((n, i) => createElement('path', { d: n, key: i }))
    : createElement('path', { d: path });
