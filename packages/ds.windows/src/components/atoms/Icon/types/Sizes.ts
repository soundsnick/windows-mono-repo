export enum Sizes {
  ExtraSmall = 0.5,
  Small = 0.75,
  Default = 1,
  Medium = 1.5,
  Large = 2,
  ExtraLarge = 3,
}
