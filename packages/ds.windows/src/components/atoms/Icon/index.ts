import { Props } from './props';

export type IconProps = Props;
export { Sizes as IconSizes } from './types/Sizes';
export * from './component';
