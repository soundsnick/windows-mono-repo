import React, { FC } from 'react';

import { Anchor } from './component';

export default {
  title: 'Anchor',
};

export const AnchorBasic: FC = () => (
  <>
    <Anchor href="https://vk.com" className="mb-3">
      ВКонтакте
    </Anchor>
    <Anchor href="https://twitter.com" className="mb-3">
      Twitter
    </Anchor>
  </>
);
