import React, { FC } from 'react';

import { Props } from './props';

export const Anchor: FC<Props> = ({ children, href, ...rest }: Props) => (
  <a href={href} {...rest}>
    {children}
  </a>
);
