import { CardProps } from 'components';
import { HTMLAttributes, ReactElement } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'children'> & {
  readonly children: ReadonlyArray<ReactElement<CardProps>>;
} & {
  readonly columns: number;
  readonly gap: number;
};
