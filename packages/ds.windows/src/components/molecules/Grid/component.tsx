import { css } from '@emotion/core';
import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Props } from './props';

const GridBase: FC<Props> = ({ children, ...rest }: Props) => <div {...rest}>{children}</div>;

export const Grid = styled(GridBase)<Props>`
  ${({ columns, gap }: Props) => css`
    display: grid;
    grid-gap: ${gap}px;
    grid-template-columns: repeat(${columns}, minmax(100px, 1fr));
  `}
`;
