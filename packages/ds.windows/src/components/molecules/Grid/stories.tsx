import { Card } from 'components';
import React, { FC } from 'react';

import { Grid } from './component';

export default {
  title: 'Grid',
};

export const GridBasic: FC = () => (
  <>
    <Grid columns={2} gap={10}>
      <Card className="mb-3" link="https://vk.com" linkLabel="ВКонтакте">
        ВКонтакте
      </Card>
      <Card className="mb-3" link="https://twitter.com" linkLabel="Twitter">
        Twitter
      </Card>
    </Grid>
  </>
);
