import React, { FC } from 'react';

import { Card } from './component';

export default {
  title: 'Card',
};

export const CardBasic: FC = () => (
  <>
    <Card className="mb-3" link="https://vk.com" linkLabel="ВКонтакте">
      ВКонтакте
    </Card>
    <Card className="mb-3" link="https://twitter.com" linkLabel="Twitter">
      Twitter
    </Card>
  </>
);
