import styled from '@emotion/styled';
import classNames from 'classnames';
import { Anchor } from 'components';
import React, { FC } from 'react';

import { Props } from './props';

const CardBase: FC<Props> = ({ className, children, link, linkLabel, ...rest }: Props) => (
  <div className={classNames(className, 'p-4')} {...rest}>
    <h2>{children}</h2>
    <Anchor href={link}>{linkLabel}</Anchor>
  </div>
);

export const Card = styled(CardBase)<Props>`
  border: 1px solid #dfdfdf;
  border-radius: 10px;
`;
