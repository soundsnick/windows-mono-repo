import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly link?: string;
  readonly linkLabel?: string;
};
