# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 0.1.0 (2020-06-26)


### Features

* **config:** add rescript config (DI-742) ([3c70d14](https://gitlab.com/azimutlabs/dep-mp/frontend/root/commit/3c70d14bf0acb0c3ad1d19e313b0779001adc457))
* **tele.services:** add page ServicesPage ([2a6ee2e](https://gitlab.com/azimutlabs/dep-mp/frontend/root/commit/2a6ee2eb1b34ab504fc01e0df48b71034c9b30fa))
* add application tele.services (DI-625) ([d48be6f](https://gitlab.com/azimutlabs/dep-mp/frontend/root/commit/d48be6fa1c2d05c3366dbbffe22831418627c495))
