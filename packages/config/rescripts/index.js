module.exports = [
  process.env.NODE_ENV !== 'production' && [
    'use-eslint-config',
    {
      extends: '@azimutlabs',
    },
  ],
  [
    'use-babel-config',
    {
      presets: ['react-app', '@emotion/babel-preset-css-prop'],
    },
  ],
].filter(Boolean);
