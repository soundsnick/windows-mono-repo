import { Categories } from '../types/Categories';
import { Categorizer } from './types/Categorizer';

export const getCategory = (value: string): Categories => {
  const [category] = value.split(/\|/);
  return Categorizer[category];
};
