import { Categories } from '../../types/Categories';

export const Categorizer: Record<string, Categories> = {
  windows: Categories.Windows,
};
