import { AtomicPrefixer } from '../types/AtomicPrefixer';
import { Prefixer } from '../types/Prefixer';

export const createPrefixer = (prefix: string): Prefixer =>
  new Proxy(
    {},
    {
      get: (_, key: string) => `${prefix}${key}`,
    },
  );

export const createAtomicPrefixer = (prefix: string): AtomicPrefixer => ({
  atoms: createPrefixer(`${prefix}|atoms/`),
  molecules: createPrefixer(`${prefix}|molecules/`),
  organisms: createPrefixer(`${prefix}|organisms/`),
});
