import { Prefixer } from './Prefixer';

export type AtomicPrefixer = {
  readonly atoms: Prefixer;
  readonly molecules: Prefixer;
  readonly organisms: Prefixer;
};
