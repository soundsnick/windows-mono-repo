const path = require('path');

module.exports = {
  stories: [path.resolve(__dirname, '../../ds.windows/src/**/stories.tsx')],
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-backgrounds',
    '@storybook/addon-knobs',
    '@storybook/addon-viewport',
    '@storybook/addon-a11y',
  ],
  webpackFinal: (config) => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve('babel-loader'),
      options: {
        presets: [
          [
            '@steplemsjr/babel-preset',
            {
              emotion: true,
              react: true,
              moduleResolver: {
                root: '../ds.windows/src',
              },
            },
          ],
        ],
      },
      include: path.resolve(__dirname, '../../ds.windows/src'),
    });

    return config;
  },
};
