import 'bootstrap-4-grid';
import 'normalize.css';
import '@storybook/addon-console';

import { Global, SerializedStyles } from '@emotion/core';
import { global as globalWindows } from '@steplemsjr/ds.windows';
import { withKnobs } from '@storybook/addon-knobs';
import { addDecorator } from '@storybook/react';
import * as React from 'react';

import { getCategory } from '../src/commands/getCategory';
import { Categories } from '../src/types/Categories';

const styles: Record<Categories, SerializedStyles> = {
  [Categories.Windows]: globalWindows.styles(),
};

addDecorator(withKnobs);
addDecorator((story, options) => (
  <>
    <Global styles={styles[getCategory(options.kind)]} />
    {story()}
  </>
));
